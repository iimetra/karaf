package com.iimetra.rest2jms;

import org.apache.camel.builder.RouteBuilder;

public class RestToJmsRoute extends RouteBuilder {
    @Override
    public void configure() throws Exception {
        restConfiguration().component("restlet").port("9181");
        rest("/say")
                .get("/hello/{name}")
                .route()
                .transform()
                .simple("Hello ${header.name}")
                .log("TO JMS: ${body}")
                .id("restGetHelloToJms")
                .to("jms://queue:hello-get-request");
        from("jms://queue:hello-get-request")
                .log("FROM JMS: ${body}")
                .id("getFromJms");

        rest("/message")
                .post()
                .consumes("application/json")
                .produces("application/json")
                .route()
                .log("TO JMS: ${body}")
                .id("restPostMessageToJms")
                .to("jms://queue:message-post-request");
        from("jms://queue:message-post-request")
                .log("FROM JMS: ${body}")
                .id("postFromJms");
    }
}
