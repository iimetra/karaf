package com.iimetra.rest2jms;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.component.jms.JmsConfiguration;
import org.apache.camel.impl.DefaultCamelContext;

public class RestToJmsStarter {
    public void run() throws Exception {
        CamelContext context = new DefaultCamelContext();
        JmsConfiguration jmsConfig =
                new JmsConfiguration(
                        new ActiveMQConnectionFactory("tcp://localhost:61616"));
        context.addComponent("jms", new JmsComponent(jmsConfig ));
        context.setTracing(true);
        context.addRoutes(new RestToJmsRoute());
        context.start();
    }

    public static void main(String[] args) throws Exception {
        RestToJmsStarter starter = new RestToJmsStarter();
        starter.run();
    }
}
